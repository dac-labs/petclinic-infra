# Petclinic

This is a continuation of the DevOps and Cloud Labs Project. This project is responsible for providing the aws infrastructure will see the usage of two previous pipeline we created.

Petclinic Repo: https://github.com/spring-projects/spring-petclinic

AMI Pipeline: https://gitlab.com/dac-labs/petclinic

Petclinic Pipeline: https://gitlab.com/dac-labs/aws-ami

The project was project challenge from a DevOps community named DevOps and Cloud Labs, anyone can join us at https://www.youtube.com/@DevOps-Cloud. We are exited and we will wait for you there.

## Objectives

#### 1. Use terraform to create the AWS Infrastructure

The terraform project must use S3 bucket as the backend storage and dynamo db for state locking.

NOTE: In this project, AWS S3 bucket named "petclinic" is used store the file "terraform.tfstate" and AWS DynamoDB table "terraform_state_lock" is created manually in the AWS tenant account. The "terraform.tfstate" file will contain the state of the aws infrastructure.

NOTE: The AWS DynamoDB table "terraform_state_lock" is created with a Partition Key (Basically a Simple Primary Key) value of LockID. This partition key is required by terraform for the state locking to work.

#### 2. On commit to any branch terraform init, validate and fmt is ran.

#### 3. On Merge to main, terraform apply is ran.

## Pipeline Job Documentation

#### 1. Create required files

Job "create required values" creates the "user.tfvars" and "scripts/deploy-petclinic-files.yaml" relative to the root directory.

user.tfvars - using envsubst the job will read the contents of templates/user_template.tfvars and replace all variables with the actual values store in the pipeline variables.

scripts/deploy-petclinic-files.yaml - By running the "scripts/deploy-petclinic.sh", it creates the file "scripts/deploy-petclinic-files.yaml". This yaml file will be used as the cloud init file to perform initial logic to the EC2 instances. This is basically the startup script when EC2 instance is spawn.

settings.xml is a required file that maven uses to publish the .jar file to the pipeline package registry.

This pipeline job will ran at any branch.

#### 2. Perform terraform initialize and plan

Job "terraform initialize and plan" is responsible downloading the terraform plugins that the terraform project requires to create the AWS infrastructure. It will also print out the planned changes in the pipeline output.

This pipeline job will ran at any branch.

#### 3. Perform terraform apply on main branch

Job "terraform apply" will read all the \*.tf files and will figure out the dependencies including the custom ones.

## Acknowledgements

- Thank you to the Admins of the group at [DevOps and Cloud Labs](https://awesomeopensource.com/project/elangosundar/awesome-README-templates) Channel on giving us beginner friendly projects that allows us to push the limits on what we can do despite lacking expience.
