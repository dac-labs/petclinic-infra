terraform {
  backend "s3" {
    bucket         = "petclinic"
    key            = "terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "terraform_state_lock"
    encrypt        = "true"
  }
}
